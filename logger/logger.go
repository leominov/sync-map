package logger

import (
	"fmt"
	"sync"

	"github.com/sirupsen/logrus"
)

var (
	deduplicationMap = &sync.Map{}
)

func InfoOnce(format string, args ...interface{}) {
	message := fmt.Sprintf(format, args...)
	if _, loaded := deduplicationMap.LoadOrStore(message, true); !loaded {
		logrus.Info(message)
	}
}

func Info(format string, args ...interface{}) {
	logrus.Infof(format, args...)
}
