package main

import "gitlab.com/leominov/sync-map/logger"

func main() {
	logger.Info("foo=%d", 1)

	logger.InfoOnce("foo=%d", 2)
	logger.InfoOnce("foo=%d", 2)
	logger.InfoOnce("foo=%d", 2)
	logger.InfoOnce("foo=%d", 2)
}
